import java.io.*;
import java.util.*;
class Hello {
	public static void main(String[] args) {
		System.out.println("Hello World");
		System.out.println("Its Me Again");
		System.out.println("1: Add");
		System.out.println("2: Subtract");
		System.out.println("3: Multiply");
		System.out.println("4: Divide");
		takeUserInput(new Calculator());

	}

	private static void takeUserInput(Calculator cal) {
		Scanner sc=new Scanner(System.in);
		System.out.print("Operation: ");
		int op=sc.nextInt();
		System.out.print("Input: ");
		int a =sc.nextInt();
		int b=sc.nextInt();
		switch(op) {
			case 1:
				System.out.println(cal.add(a,b));
				break;
			case 2:
				System.out.println(cal.diff(a,b));
				break;
			case 3:
				System.out.println(cal.mul(a,b));
				break;
			case 4:
				System.out.println(cal.div(a,b));
				break;	
		}

	}
}